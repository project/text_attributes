<?php

namespace Drupal\text_attributes\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Create a new formatter for text field.
 *
 * @FieldFormatter(
 *   id = "text_field_formatter",
 *   label = @Translation("Text field formatter"),
 *   field_types = {
 *     "text_attribute"
 *   }
 * )
 */
class TextFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Displays the added functionalities by module.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $key => $item) {
      $markup = "<div ";
      if (!empty($item->options)) {
        foreach ($item->options['attributes'] as $delta => $attribute) {
          $value = (is_array($attribute)) ? implode(" ", $attribute) : $attribute;
          $markup .= $delta . '="' . strtolower($value) . '" ';
        }
      }
      $markup .= ">" . $item->text . "</div>";
      $elements[$key] = [
        "#markup" => $markup,
      ];
    }
    return $elements;
  }

}
