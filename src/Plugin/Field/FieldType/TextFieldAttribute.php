<?php

namespace Drupal\text_attributes\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\MapDataDefinition;

/**
 * Plugin extending text field type.
 *
 * @FieldType(
 *   id = "text_attribute",
 *   label = @Translation("Text Attribute"),
 *   description = @Translation("Stores a string, and optional blob of attributes."),
 *   default_widget = "text_field_widget",
 *   default_formatter = "text_field_formatter",
 *   category = @Translation("Text"),
 * )
 */
class TextFieldAttribute extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
        'options' => DRUPAL_OPTIONAL,
      ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties['text'] = DataDefinition::create('string')
      ->setLabel(t('Text'));

    $properties['options'] = MapDataDefinition::create()
      ->setLabel(t('Options'));
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'text' => [
          'description' => 'The text.',
          'type' => 'varchar',
          'length' => 255,
        ],
        'options' => [
          'description' => 'Serialized array of options.',
          'type' => 'blob',
          'size' => 'big',
          'serialize' => TRUE,
        ],
      ],
      'indexes' => [
        'text' => [['text', 30]],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = [];

    $element['options'] = [
      '#type' => 'radios',
      '#title' => t('Allow Text Options'),
      '#default_value' => $this->getSetting('options'),
      '#options' => [
        DRUPAL_DISABLED => t('Disabled'),
        DRUPAL_OPTIONAL => t('Optional'),
        DRUPAL_REQUIRED => t('Required'),
      ],
    ];

    return $element;
  }


  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('text')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'text';
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    // Treat the values as property value of the main property, if no array is
    // given.
    if (isset($values) && !is_array($values)) {
      $values = [static::mainPropertyName() => $values];
    }
    if (isset($values)) {
      $values += [
        'options' => [],
      ];
    }
    // Store options as a string.
    if (is_string($values['options'])) {
      if (version_compare(PHP_VERSION, '7.0.0', '>=')) {
        $values['options'] = unserialize($values['options'], ['allowed_classes' => FALSE]);
      }
      else {
        $values['options'] = unserialize($values['options']);
      }
    }
    parent::setValue($values, $notify);
  }

}
