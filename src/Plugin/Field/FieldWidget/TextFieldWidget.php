<?php

namespace Drupal\text_attributes\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'text_field_widget' widget.
 *
 * @FieldWidget(
 *   id = "text_field_widget",
 *   label = @Translation("Text field widget"),
 *   field_types = {
 *     "text_attribute"
 *   }
 * )
 */
class TextFieldWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'size' => 60,
      'placeholder' => '',
      'id' => '',
      'class' => '',
      'enabled_attributes' => [
        'id' => TRUE,
        'class' => TRUE,
      ],
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);
    $options = $this->getAttributes();
    $selected = $this->getSetting('enabled_attributes');
    $element['enabled_attributes'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled attributes'),
      '#options' => $options,
      '#default_value' => $selected,
      '#description' => $this->t('Select the attributes to allow the user to edit.'),
    ];
    return $element;

  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $enabled_attributes = array_filter($this->getSetting('enabled_attributes'));
    if ($enabled_attributes) {
      $summary[] = $this->t('With attributes: @attributes', ['@attributes' => implode(', ', array_keys($enabled_attributes))]);
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $item = $items[$delta];
    $element['text'] = $element + [
      '#type' => 'textfield',
      '#default_value' => !empty($item->text) ? $item->text : NULL,
      '#size' => $this->getSetting('size'),
      '#placeholder' => $this->getSetting('placeholder'),
      '#maxlength' => $this->getFieldSetting('max_length'),
      '#attributes' => ['class' => ['js-text-full', 'text-full']],
    ];
    $attributes = $this->getSetting('enabled_attributes');
    $element['options']['attributes'] = [
      '#type' => 'details',
      '#title' => $this->t('Attributes'),
      '#tree' => TRUE,
      '#open' => count($attributes),
    ];
    // Add activated attributes.
    foreach ($this->getAttributes() as $key => $attribute) {
      if (!empty($attribute)) {
        $value = !empty($item->options['attributes'][$key]) ? $item
          ->options['attributes'][$key] : NULL;
        $element['options']['attributes'][$key] = [
          '#title' => $attribute,
          '#type' => 'textfield',
          '#empty_value' => '',
          '#validated' => TRUE,
          '#default_value' => $value,
        ];
      }
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    // Convert a class string to an array so that it can be merged reliable.
    foreach ($values as $key => $value) {
      if (isset($value['options']['attributes']['class']) && is_string($value['options']['attributes']['class'])) {
        $values[$key]['options']['attributes']['class'] = explode(' ', $value['options']['attributes']['class']);
      }
    }

    return array_map(function (array $value) {
      if (isset($value['options']['attributes'])) {
        $value['options']['attributes'] = array_filter($value['options']['attributes'], function ($attribute) {
          return $attribute !== "";
        });
      }
      return $value;
    }, $values);
  }

  /**
   * Return attribute names.
   */
  private function getAttributes() {
    return [
      'id' => 'ID',
      'class' => t('CSS class name(s)'),
    ];
  }

}
