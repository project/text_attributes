<?php

namespace Drupal\text_attributes;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Discovery\YamlDiscovery;
use Drupal\Core\Plugin\Discovery\ContainerDerivativeDiscoveryDecorator;

/**
 * Class TextAttributesManager.
 *
 * @package Drupal\text_attributes
 */
class TextAttributesManager extends DefaultPluginManager implements PluginManagerInterface {

  /**
   * Provides default values for all text_attributes plugins.
   *
   * @var array
   */
  protected $defaults = [
    'title' => '',
    'type' => '',
    'description' => '',
  ];

  /**
   * Constructs a TextAttributesManager object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   */
  public function __construct(ModuleHandlerInterface $module_handler, CacheBackendInterface $cache_backend) {
    $this->alterInfo('text_attributes_plugin');
    $this->moduleHandler = $module_handler;
    $this->setCacheBackend($cache_backend, 'text_attributes', ['text_attributes']);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDiscovery() {
    if (empty($this->discovery)) {
      $this->discovery = new YamlDiscovery('text_attributes', $this->moduleHandler->getModuleDirectories());
      $this->discovery->addTranslatableProperty('title');
      $this->discovery = new ContainerDerivativeDiscoveryDecorator($this->discovery);
    }
    return $this->discovery;
  }

  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $plugin_id) {
    parent::processDefinition($definition, $plugin_id);

    // Make sure each plugin definition had at least a field type.
    if (empty($definition['type'])) {
      $definition['type'] = 'textfield';
    }
  }

}
